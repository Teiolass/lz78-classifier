% \documentclass[aspectratio=43, handout]{beamer}
\documentclass[aspectratio=43]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{superpresent}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{bold-extra}
\usepackage{listings}
\usepackage{subcaption}

\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}

\renewcommand{\c}[1]{\text{\texttt{#1}}}
\newcommand{\ind}{\hspace{0.7cm}}
\newcommand{\fl}{\text{fl}}
\definecolor{evidence_color_1}{RGB}{242, 127, 101}
\definecolor{evidence_color_2}{RGB}{149, 92, 165}
\definecolor{evidence_color_3}{RGB}{72, 83, 149}
\definecolor{evidence_color_4}{RGB}{242, 187, 121}
\definecolor{evidence_color_5}{RGB}{242, 174, 224}


\title[]
    {Classificazione di Testi con Tecniche\\ di Teoria dell'Informazione}
\subtitle{}
\author{Alessio Marchetti}
% \institute{Universit\`a di Pisa, Dipartimento di Matematica}
\date{\textit{\small{6 Maggio 2022}}}


\begin{document}
\frame{\titlepage}

\begin{frame}
    \frametitle{Scopo}
    Lo scopo di questa presentazione \`e quello di sviluppare un algoritmo di
    apprendimento supervisionato per la classificazione binaria per testi.
    \\~\

    Utilizzeremo come esempio un dataset comprendente messaggi classificati come
    di \textit{spam} e \textit{non spam}.
    \\~\

    Vedremo come la teoria dell'informazione fornisce strumenti utili e
    algoritmi di compressione possano essere usati per ottenere classificazioni
    rapide e accurate.
\end{frame}

\begin{frame}
    \frametitle{Definizioni Preliminari e Propriet\`a di Base (1/2)}
    Definiamo l'entropia di una v.a. $X$ a valori in $E$ come
    $$H(X) = -\sum_{x\in E} p(x) \log{p(x)}.$$

    \begin{itemize}
        \item L'entropia di una variabile aleatoria non negativa a valori
            interi a valore atteso fissato $\mu$ \`e massima quando essa \`e
            geometrica e vale $(\mu+1)\log(\mu+1) - \mu\log \mu$.
    \end{itemize}
    \vspace{0.5cm}

    Siano $X,Y$ v.a. a valori in $E$ con legge congiunta $p$. L'entropia
    condizionale \`e $H(X|Y) = -\sum_{x,y\in E} p(x,y)\log p(x|y)$.

    \begin{itemize}
        \item Vale la chain rule: $H(X,Y) = H(X) + H(Y|X)$. Si generalizza anche
            a pi\`u variabili.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Definizioni Preliminari e Propriet\`a di Base (2/2)}
    Siano $p,q$ misure di probabilit\`a su $E$. Definiamo la loro distanza
    (divergenza) di Kullback-Leibler come 
    $$D(p || q) = \sum_{x\in E} p(x) \log{\frac{q(x)}{p(x)}}.$$
    Essa \`e non negativa e nulla se e solo se $p=q$. Non \`e per\`o una
    distanza.
    \\~\

    Il tasso di entropia di un processo stocastico $(X_n)_{n\in \mathbb{N}}$ \`e
    $$H(X) = \lim_{n\rightarrow\infty}\frac{1}{n}H(X_1, \dots, X_n) = 
    \lim_{n\rightarrow\infty}H(X_n|X_{n-1}, \dots, X_1).$$
\end{frame}

\begin{frame}
    \frametitle{Codici}
    Un codice non singolare per una variabile aleatoria $X$ a valori in $E$ \`e
    una mappa iniettiva $c: E \rightarrow \mathcal{D}^*$, dove $\mathcal{D}^*$
    \`e l'insieme delle stringhe sull'alfabeto $\mathcal{D}$. 
    \\~\

    Diciamo che un codice \`e unicamente decodificabile se nessuna codifica \`e
    prefisso di un'altra. Ovvero l'estensione del codice a $E^*$ definita da
    $$c(x_1\dots x_n) = c(x_1)\dots c(x_n)$$
    \`e ancora un codice non singolare.
    \\~\

    Dato un elemento $x\in E$, $l(x)$ \`e la lunghezza della codifica $c(x)$. La
    lunghezza attesa di un codice per $X$ \`e $L(c)= \mathbb{E}[l(X)]$.
\end{frame}

\begin{frame}
    \frametitle{Codifiche Ottimali}
    \begin{tcolorbox}[title=Teorema]
    I codici unicamente decodificabili soddisfano la disuguaglianza di Kraft:
    $$\sum_{x\in E} |\mathcal{D}|^{-l(x)} \leq 1.$$
    \end{tcolorbox}
    \vspace{0.2cm}

    Ottimizzando la lunghezza attesa $L$ variando le lunghezze $l(x)$ soggette
    alla disuguaglianza di Kraft, si ottengono che le lunghezze ottimali sono
    $l^*(x) = - \log_{|\mathcal{D}|} p_X(x)$, che porta ad avere la lunghezza
    attesa $L^*(c) = H(X)$.
    \\~\

    Esistono codici con lunghezze $l(x) = \lceil l^*(x) \rceil$ che portano ad
    avere $H(X) \leq L(c) \leq H(X)+1$ (codici di Shannon).
\end{frame}

\begin{frame}
    \frametitle{Codifiche di Processi Stazionari}
    Possiamo per\`o codificare $n$-uple di variabili aleatorie indipendenti con
    un codice $c_n$ tale che
    $$ H(X_1, \dots, X_n) \leq L(c_n) \leq H(X_1, \dots, X_n)  + 1.$$
    
    Per cui, per $n\rightarrow \infty$, $L(c_n)/n \rightarrow H(X)$.
\end{frame}

\begin{frame}
    \frametitle{Codifiche Non Ottimali}
    Consideriamo due v.a. $X$, $Y$ a valori nello stesso insieme finito con
    distribuzioni $p$ e $q$. 

    Sia $c$ un codice per cui $l(x) = \lceil -\log q(x) \rceil$.

    Allora la lunghezza attesa di $c$ per codificare $X$ \`e
    \begin{align*}
        \mathbb{E}[l(X)] &= \sum_x p(x)\lceil -\log q(x) \rceil \\
        &<\sum_x p(x) \log \frac{p(x)}{q(x)} + \sum_x p(x) \log \frac{1}{p(x)} +
        1 \\ &= D(p||q) + H(X) + 1.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{L'algoritmo LZ78 (Lempel-Ziv)}
    Supponiamo di avere una stringa, ad esempio \texttt{ABBABAA}.
    \\~\

    La comprimiamo dividendola in frasi uniche di minima lunghezza:
    \texttt{A, B, BA, BAA}.
    \\~\
    
    Ciascuna di queste parti \`e la concatenzione di una frase precedente e un
    carattere. Quindi possiamo codificare ogni frase come una coppia formata
    dall'indice del prefisso e dal carattere nuovo.
    \\~\

    La codifica sar\`a dunque \texttt{(0, A), (0, B), (2, A), (3, A)}.
    \\~\

    Osserviamo che la codifica \`e fatta senza conoscere la distribuzione del
    processo.
\end{frame}

\begin{frame}
    \frametitle{Frasi Distinte}
    Consideriamo una stringa di lunghezza $n$. Sia $c(n)$ il numero di frasi
    distinte nella stringa.
    \\~\

    Osserviamo che la codifica del puntatore di LZ78 necessita $\log c(n)$ bits,
    e dunque la lunghezza attesa della codifica \`e $c(n) (\log c(n) + 1)$.
    \\~\

    \begin{tcolorbox}[title=Lemma]
    Vale che
    $$ c(n) \leq \frac{n}{(1-\epsilon_n)\log{n}} $$
    dove $\epsilon_n\rightarrow 0$ per $n \rightarrow\infty$.
    \end{tcolorbox}
    \vspace{0.2cm}
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione (1/2)}
    Sia $n_k$ la somma delle lunghezze di tutte le possibili stringhe di
    lunghezza al pi\`u $k$, per cui $n_k = \sum_{i=1}^k i 2^i = (k-1)2^{k+1} +
    2$.
    \\~\

    Inoltre
    $$c(n_k) = \sum_{i=1}^k 2^i < 2^{k+1} \leq \frac{n_k}{k-1}.$$
    \\~\

    Supponiamo $n_k\leq n < n_{k+1}$. Allora il parsing ottimale \`e composto da
    $c(n_k)$ frasi di lunghezza al pi\`u $k$, e $n-n_k$ di lunghezza $k+1$.
    Allora
    $$c(n) \leq \frac{n_k}{k-1} + \frac{n-n_k}{k+1} \leq \frac{n}{k-1}.$$
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione (2/2)}
    Inoltre $n \geq n_k$ implica $k\leq \log n$.
    \\~\

    Similmente $n\leq n_{k+1}$ implica $n\leq (k+1)2^{k+2} \leq (\log n +
    2)2^{k+2}$ da cui otteniamo 
    \begin{align*}
        k-1 &\geq \left( 1 - \frac{\log\log n + 4}{\log n}\right) \log n \\
        &= (1-\epsilon_n)\log n.
    \end{align*}
    \\~\

    Otteniamo quindi la tesi $c(n) \leq \frac{n}{(1-\epsilon_n)\log n}$.
\end{frame}

\begin{frame}
    \frametitle{Approssimazioni Markoviane}
    Data una successione $x_n$, definiamo $x_n^m = (x_n, x_{n+1}, \dots x_m)$.
    \\~\

    Sia $(X_n)_{n=-\infty}^{+\infty}$ un processo ergodico e stazionario a
    valori $E$. Definiamo la sua approssimazione markoviana di ordine
    $k\in\mathbb{N}$ come la distribuzione definita da
    $$
        Q_k(x_{-k+1}^n) = P(x_{-k+1}^0)\prod_{j=1}^n P(x_j|x_{j-k}^{j-1}).
    $$

\end{frame}

\begin{frame}
    \frametitle{Disuguaglianza di Ziv}
    Sia $y_1, \dots, y_c$ un parsing distinto di $x_1, \dots, x_n$, dove $y_i =
    x_{\nu_i}^{\nu_{i+1}-1}$. Siano $s_i = x_{\nu_i-k}^{\nu_i-1}$, per tutti gli
    $i$.
    \\~\

    Per ogni stringa $s\in E^k$ e intero $l$, definiamo $c_{l,s}$ come il numero
    di frasi $y_i$ di lunghezza $l$ per cui $s_i=s$.
    \\~\

    \begin{tcolorbox}[title=Teorema (Disuguaglianza di Ziv)]
        Vale
        $$
            \log Q_k(x_1^n|s_1) \leq - \sum_{l,s} c_{l,s}\log c_{l,s}.
        $$
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione}
    Poich\'e $Q_k(x_1^n|s_1) = \prod_{i=1}^c P(y_i|s_i)$,
    \begin{align*}
        \log Q(x_1^n|s_1) &= \sum_{i=1}^c \log P(y_i|s_i) \\
        &= \sum_{l,s} c_{l,s} \sum_{i: |y_i| = l, s_i = s} \frac{1}{c_{l,s}}\log
            P(y_i|s_i) \\
        &\leq \sum_{l,s}c_{l,s} \log \left(\sum_{i: |y_i| = l, s_i = s}
            \frac{1}{c_{l,s}} P(y_i|s_i) \right).
    \end{align*}
    Nell'ultimo passaggio si \`e usata la disuguaglianza di Jensen. 
    
    Concludiamo osservando che tutte le $y_i$ sono distinte.
\end{frame}

\begin{frame}
    \frametitle{Ottimalit\`a di LZ78}
    \begin{tcolorbox}[title=Teorema]
        Sia $(X_n)$ un processo ergodico stazionario, e sia $c(n)$ il numero di
        frasi distinte nel parsing di $X_1^n$. Allora q.c.
        $$\limsup_{n\rightarrow \infty} \frac{c(n)\log c(n)}{n} \leq H(X).$$
    \end{tcolorbox}
    \vspace{0.3cm}

    L'ottimalit\`a di LZ78 segue perch\'e la lunghezza della codifica era
    ${c(n)[\log c(n)+1]}/{n}$.
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione (1/3)}
    Osserviamo che $\sum_{l,s} c_{l,s} = c$ e che $\sum_{l,s} lc_{l,s} = n$.
    \\~\

    Definiamo allora due v.a. $U,V$ tali che
    $$P(U=l, V=s) = \frac{c_{l,s}}{c}.$$

    Ne consegue che $\mathbb{E}[U] = n/c$, per cui 
    $$
    H(U) \leq \left(\frac{n}{c}+1\right)\log \left(\frac{n}{c}+1\right) -
        \frac{n}{c}\log \frac{n}{c}.
    $$

    Inoltre l'immagine di $V$ conta al pi\`u $2^k$ elementi e dunque la sua
    entropia \`e limitata dall'alto da $\log 2^k=k$ bits.
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione (2/3)}
    Per la disuguaglianza di Ziv
    \begin{align*}
        \log Q_k(x_1^n|s_1) &\leq - \sum_{l,s} c_{l,s} \log c_{l,s} \\
        &= -c\log c - c \sum_{l,s} \frac{c_{l,s}}{c} \log \frac{c_{l,s}}{c} \\
        &= -c\log c + c\ H(U,V)
    \end{align*}
    in cui sappiamo controllare $H(U,V) \leq H(U) + H(V)$.
    \\~\

    Inoltre abbiamo la disuguaglianza $c \leq \frac{n}{\log n}(1 + o(1))$.
\end{frame}

\begin{frame}
    \frametitle{Dimostrazione (3/3)}
    Mettendo insieme tutte le informazioni si ottiene che
    \begin{align*}
        \limsup_{n\rightarrow\infty} \frac{c(n)\log c(n)}{n} &\leq
        \lim_{n\rightarrow\infty} -\frac{1}{n} \log Q_k(X_1^n|X_{-k+1}^0) \\
        &= \lim_{n\rightarrow\infty}-\frac{1}{n}\sum_{j=1}^n \log P(X_j|X_{j-k}^{j-1})\\
        &= \mathbb{E}[\log P(X_j|X_{j-k}^{j-1})] = H(X_0|X_{-k}^{-1}) \\
        &\rightarrow H(X)
    \end{align*}
    quando $k\rightarrow \infty$.

\end{frame}

\begin{frame}
    \frametitle{Una Variante di LZ78}
    Proponiamo una variante di LZ78 in cui al posto di comprimere una stringa su
    una sua porzione precedente, essa viene compressa su una seconda stringa
    fissata. 
    \\~\

    Per esempio $x=$ \texttt{ABBA} pu\`o essere compressa su $z=$ \texttt{BBA}
    come {{\texttt{(3, 1), (1, 3)}}}.
    \\~\

    Chiamiamo $c(x|z)$ il numero di frasi ottenute dal parsing di $x$ sulla
    stringa $z$.
\end{frame}

\begin{frame}
    \frametitle{L'Algoritmo come Codifica non Ottimale}
    Possiamo vedere se $x$ e $y$ sono stringhe di lunghezza $n$, realizzazioni
    dei processi $X$, $Y$, $c(x|y)$ \`e la lunghezza di una codifica di $x$
    ottimale per il processo $Y$.
    \\~\

    \`E ragionevole pensare che questa quantit\`a sia legata strettamente a
    $D(X||Y)$.
\end{frame}

\begin{frame}
    \frametitle{Stima della Divergenza KL}
    Data una stringa $z$, chiamiamo $q_z$ la misura di probabilit\`a sulle
    stringhe di lunghezza $l$ per cui $q_z(a)$ \`e proporzionale al numero di
    volte in cui $a$ compare in $z$ come sottostringa.

    \vspace{0.4cm}
    \begin{tcolorbox}[title=Teorema (Ziv-Merhav)]
        Sia $\Delta(z||x) = \frac{1}{n}[c(z|x) \log n - c(z) \log c(z)]$.

        Allora per ogni $l$ vale quasi certamente che
        $$
            \lim_{n\rightarrow\infty}[\Delta(z||x)- D(q_z||p)]
        $$
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Classificazione}
    Per la sperimentazione ho usato un dataset di 5575 messaggi classificati
    come \textit{spam} oppure \textit{non spam}.
    \\~\

    I messaggi utilizzati per il training sono stati concatenati per ottenere un
    testo per categoria di uguale lunghezza $s_\text{spam}$ e $s_\text{ham}$. La
    classificazione di una stringa $t$ avviene confrontando $c(t|s_\text{spam})$
    e $c(t|s_\text{ham})$.
    \\~\

    L'algoritmo raggiunge un'accuratezza del 97\% sul set di test. Altri metodi
    sulla medesima classificazione ottengono risultati del 98\%.
\end{frame}

\begin{frame}
    \frametitle{Risultati}
    \begin{figure}
    \includegraphics[width=1\textwidth]{plot}
    \end{figure}
\end{frame}

\begin{frame}
    \begin{tcolorbox}
        \vspace{0.5cm}
        \centering
        Grazie per l'attenzione
        \vspace{0.5cm}
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Bibliografia}
    \bibliographystyle{plain}
    \nocite{cover_elements_2006}
    \nocite{shannon_prediction_1951}
    \nocite{basile_example_2008}
    \nocite{ziv_measure_1993}
    \bibliography{biblio}
\end{frame}


\end{document}
