#![feature(iter_map_while)]
#![feature(format_args_capture)]

use std::cmp::max;
use std::fs::File;
use std::io::Read;

mod test;

fn compress(dic: &String, s: &String) -> u16 {
    let dic = dic.replace(|c: char| !c.is_ascii(), "");
    let s = s.replace(|c: char| !c.is_ascii(), "");
    let mut count = 0_u16;
    let mut off = 0_usize;
    while off < s.len() {
        let u = (off..=s.len())
            .map_while(|i| dic.find(&s[off..i]))
            .enumerate()
            .last()
            .unwrap();
        off += max(u.0, 1);
        count += 1;
    }
    count
}

fn analyze_csv(ref_spam: &String, ref_ham: &String, path_in: &str, path_out: &str) {
    let mut spam_reader = csv::Reader::from_path(path_in).unwrap();
    let mut writer = csv::Writer::from_path(path_out).unwrap();
    let mut i = 0_usize;
    spam_reader.records().for_each(|record| {
        let mut r = record.as_ref().unwrap().iter();
        let x = r.nth(1).unwrap();
        let comp_spam = compress(ref_spam, &String::from(x));
        let comp_ham = compress(ref_ham, &String::from(x));
        let natural = x.len();
        print!("\r[{}]", i);
        i += 1;
        // println!("Final result: {} - {} - {}", comp_spam, comp_ham, natural);
        writer
            .write_record(&[
                comp_spam.to_string(),
                comp_ham.to_string(),
                natural.to_string(),
            ])
            .unwrap();
    });
}

fn main() {
    println!("Starting compression...");
    let mut file_ref_spam = File::open("ref_spam").unwrap();
    let mut ref_spam = String::new();
    file_ref_spam.read_to_string(&mut ref_spam).unwrap();
    let mut file_ref_ham = File::open("ref_ham").unwrap();
    let mut ref_ham = String::new();
    file_ref_ham.read_to_string(&mut ref_ham).unwrap();

    analyze_csv(&ref_spam, &ref_ham, "spams.csv", "results_spam.csv");
    analyze_csv(&ref_spam, &ref_ham, "hams.csv", "results_ham.csv");
    println!("\n Ok");
}
