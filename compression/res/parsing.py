# coding: utf-8

import pandas as pd

def parse_and_analyze():
    res_spam = pd.read_csv('results_spam.csv', header=None)
    res_ham = pd.read_csv('results_ham.csv', header=None)
    res_spam['category'] = 'spam'
    res_ham['category'] = 'ham'
    res = pd.concat([res_spam, res_ham])
    res['prediction_spam'] = res[0] < res[1]
    things = res['prediction_spam'] == (res['category'] == 'spam')
    spam_as_spam = len(res[res['prediction_spam'] & (res['category'] == 'spam')])
    ham_as_spam = len(res[res['prediction_spam'] & (res['category'] == 'ham')])
    spam_as_ham = len(res[~res['prediction_spam'] & (res['category'] == 'spam')])
    ham_as_ham = len(res[~res['prediction_spam'] & (res['category'] == 'ham')])
    return [spam_as_spam, ham_as_spam, spam_as_ham, ham_as_ham]

