from data_creation import *
from parsing import *
import os
import numpy as np

def pipe(frac=1.0):
    ref_size = create_data(data, frac=frac, K=250)
    os.system('../target/release/compression')
    confusion = parse_and_analyze()
    return (ref_size, confusion)

def run():
    fracs = np.linspace(0.1, 1, 20)
    results = []
    runs_per_frac = 5
    total_runs = runs_per_frac * len(fracs)
    runs_done = 0
    for frac in fracs:
        for i in range(runs_per_frac):
            (size, confusion) = pipe(frac=frac)
            results.append([size] + confusion)
            runs_done += 1
            print("Runs: [{} / {}]".format(runs_done, total_runs))
    df = pd.DataFrame(results, columns=['ref_size', 'spam_as_spam', \
        'spam_as_ham', 'ham_as_spam', 'ham_as_ham'])
    return df

def additional(results):
    results['total'] = results.iloc[:, 1:].sum(axis=1)
    results.head()
    results['correct'] = results['spam_as_spam'] + results['ham_as_ham']
    results['accuracy'] = results['correct'] / results['total']
    results['precision'] = results['spam_as_spam'] / \
        (results['spam_as_spam'] + results['ham_as_spam'])
    results['recall'] = results['spam_as_spam'] / (results['spam_as_spam'] \
        + results['spam_as_ham'])
    results['f1'] = 2 / (1/results['precision'] + 1/results['recall'])
    return results
