# coding: utf-8
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from math import floor

data = pd.read_csv("data.csv")
data = data.drop_duplicates()

def create_data(data, K=150, frac=1.0, shuffle=True):
    spams = data[data['Category']=='spam']['Message']
    hams = data[data['Category']=='ham']['Message']
    if shuffle:
        spams = spams.sample(frac=1.0)
        hams = hams.sample(frac=1.0)
    spam_ref = ''
    for i in range(K):
        spam_ref += spams.iloc[i]
    spam_tests = spams.iloc[K:]
    N = floor(len(spam_ref) * frac)
    ham_ref = ''
    i = 0
    while len(ham_ref) < N:
        t = hams.iloc[i]
        k = min(len(t), N - len(ham_ref))
        ham_ref += t[:k]
        i += 1
    ham_tests = hams.iloc[i:]
    with open('ref_ham', 'w+') as file:
        file.write(ham_ref)
        
    with open('ref_spam', 'w+') as file:
        file.write(spam_ref)
    ham_tests.to_csv('hams.csv')
    spam_tests.to_csv('spams.csv')
    return N

