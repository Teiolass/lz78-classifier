# coding: utf-8

res_spam = pd.read_csv('results_spam.csv', header=None)
res_ham = pd.read_csv('results_ham.csv', header=None)
res_spam['category'] = 'spam'
res_ham['category'] = 'ham'
res = pd.concat([res_spam, res_ham])
res['spam_compression'] = res[0] / res[2]
res['ham_compression'] = res[1] / res[2]
res['prediction'] = res['spam_compression'] < res['ham_compression']
things = res['prediction'] == (res['category'] == 'spam')
